
# List of queries

- [Q1.1 Return polymers that a specific membrane contains](#q11-return-polymers-that-a-specific-membrane-contains)
- [Q2.1 Return all polymers and monomers they contain](#q21-return-all-polymers-and-monomers-they-contain)
- [Q2.2 Return monomers that a specific polymer contains](#q22-return-monomers-that-a-specific-polymer-contains)
- [Q3.1 Return all subclasses of class Copolymer](#q31-return-all-subclasses-of-class-copolymer)
- [Q3.2: Return all copolymers with their classes](#q32-return-all-copolymers-with-their-classes)
- [Q4.1: Return methods used for all polymer synthesis experiments](#q41-return-methods-used-for-all-polymer-synthesis-experiments)
- [Q4.2: Return method used for a synthesis of a specific polymer](#q42-return-method-used-for-a-synthesis-of-a-specific-polymer)
- [Q4.3: Return method used in a specific polymer synthesis experiment](#q43-return-method-used-for-in-a-specific-polymer-synthesis-experiment)
- [Q4.4: Return methods used for all membrane fabrication experiments](#q44-return-methods-used-for-all-membrane-fabrication-experiments)
- [Q4.5: Return method used for a fabrication experiments of a specific membrane](#q45-return-method-used-for-a-fabrication-experiments-of-a-specific-membrane)
- [Q4.6: Return methods used for a specific membrane fabrication experiment](#q46-return-methods-used-for-a-specific-membrane-fabrication-experiment)
- [Q5.1: Return value of a specific parameter used in synthesis](#q51-return-value-of-a-specific-parameter-used-in-synthesis)
- [Q5.2: Return value of all parameters used in a specific synthesis](#q52-return-value-of-all-parameters-used-in-a-specific-synthesis)
- [Q6.1: Return value of a specific parameter used in preparation before analysis](#q61-return-value-of-a-specific-parameter-used-in-preparation-before-analysis)
- [Q7.1: Return all membranes with their classes](#q71-return-all-membranes-with-their-classes) 
- [Q7.2: Return membrane type of a specific membrane](#q72-return-membrane-type-of-a-specific-membrane)
- [Q8.1: Return all modules with their classes](#q81-return-all-modules-with-their-classes)
- [Q8.2: Return module type of a specific module](#q82-return-module-type-of-a-specific-module)
- [Q9.1: Return persons who performed experiments](#q91-return-persons-who-performed-experiments)
- [Q10.1: Return equipment used in experiments](#q101-return-equipment-used-in-experiments)
- [Q11.1: Return recorded characteristics of membranes and polymers](#q111-return-recorded-characteristics-of-membranes-and-polymers)
- [Q12.1: Return measured characteristics that calculated characteristics were derived from](#q121-return-measured-characteristics-that-calculated-characteristics-were-derived-from)
- [Q13.1: Return characteristics of polymer or membranes measured in an experiment](#q131-return-characteristics-of-polymer-or-membranes-measured-in-an-experiment)
- [Q14.1: Return all characteristics that were calculated with names of subclasses](#q141-return-all-characteristics-that-were-calculated-with-names-of-subclasses) 
- [Q14.2: Return calculated value a specific quantity](#q142-return-calculated-value-a-specific-quantity)
- [Q15.1: Return storage location of all results of a calculation](#q151-return-storage-location-of-all-results-of-a-calculation)
- [Q16.1: Return software used in a given experiment](#q161-return-software-used-in-a-given-experiment)
- [Q16.2:  Return software used in a given calculation](#q162-return-software-used-in-a-given-calculation)
- [Q17.1 Return models associated with a given experiment that were used in the experiment](#q171-return-models-associated-with-a-given-experiment-that-were-used-in-the-experiment)
- [Q18.1: Return all requests with their submission date and time](#q181-return-all-requests-with-their-submission-date-and-time)
- [Q18.2: Return submission date and time of the request with a specific request number](#q182-return-submission-date-and-time-of-the-request-with-a-specific-request-number)
- [Q19.1: Return submission date and time of a specific fabrication](#q191-return-submission-date-and-time-of-a-specific-fabrication)
- [Q19.2: Return submission date and time of a specific synthesis](#q192-return-submission-date-and-time-of-the-synthesis-with-a-specific-request-number)
- [Q19.3: Return all requests of type synthesis or fabrication with their submission date and time](#q193-return-all-requests-of-type-synthesis-or-fabrication-with-their-submission-date-and-time)


# Queries

## **Prefixes**
```sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX pmat: <https://w3id.org/polymat/>
PREFIX bfo: <http://purl.obolibrary.org/obo/>
PREFIX ex: <http://example.org/polymat_example_data#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX prov: <http://www.w3.org/ns/prov#>
```

## **Q1.1 Return polymers that a specific membrane contains**
```sparql
SELECT DISTINCT ?membrane ?polymer
WHERE {

  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?membrane_type .
  ?membrane bfo:RO_0001019 ?polymer .
  ?polymer_type rdfs:subClassOf* pmat:Polymer.
  ?polymer rdf:type ?polymer_type
 
  FILTER (?membrane=ex:polytetrafluoroethyleneMembrane5)
}
```

## **Q2.1 Return all polymers and monomers they contain**
```sparql
SELECT DISTINCT ?polymer ?monomer
WHERE {

  ?polymer_type rdfs:subClassOf* pmat:Polymer.
  ?polymer rdf:type ?polymer_type .
  ?polymer bfo:RO_0001019 ?monomer .
  ?monomer_type rdfs:subClassOf* pmat:Monomer.
  ?monomer rdf:type ?monomer_type
}
```

## **Q2.2 Return monomers that a specific polymer contains**
```sparql
SELECT DISTINCT ?polymer ?monomer
WHERE {

  ?polymer_type rdfs:subClassOf* pmat:Polymer.
  ?polymer rdf:type ?polymer_type .
  ?polymer bfo:RO_0001019 ?monomer .
  ?monomer_type rdfs:subClassOf* pmat:Monomer.
  ?monomer rdf:type ?monomer_type

FILTER (?polymer=ex:butylRubber1)
}
```

## **Q3.1 Return all subclasses of class Copolymer**
```sparql
SELECT DISTINCT ?parent_class_label ?parent_class ?class_label ?class
WHERE {
  ?class a owl:Class .
  ?class rdfs:subClassOf ?parent .
  ?parent a owl:Class .
  ?class rdfs:subClassOf ?parent_class .
  ?parent_class rdfs:label ?parent_class_label .
  FILTER (?parent_class_label = "Copolymer"@en)
}
```


## **Q3.2 Return all copolymers with their classes**
```sparql
SELECT DISTINCT ?copolymer ?copolymer_type
WHERE {

  ?copolymer_type rdfs:subClassOf* pmat:Copolymer.
  ?copolymer rdf:type ?copolymer_type .
}
```


## **Q4.1: Return methods used for all polymer synthesis experiments**
```sparql
SELECT DISTINCT ?experiment ?method_type
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:PolymerSynthesis.
  ?method rdf:type ?method_type .
  ?polymer_type rdfs:subClassOf* pmat:Polymer .
  ?polymer rdf:type ?polymer_type .
 
  ?experiment prov:generated ?polymer .
  ?experiment prov:used ?method .
}
```

## **Q4.2: Return method used for a synthesis of a specific polymer**
```sparql
SELECT DISTINCT ?experiment ?method_type ?polymer
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:PolymerSynthesis.
  ?method rdf:type ?method_type .
  ?polymer_type rdfs:subClassOf* pmat:Polymer .
  ?polymer rdf:type ?polymer_type .
 
  ?experiment prov:generated ?polymer .
  ?experiment prov:used ?method .

FILTER (?polymer=ex:butylRubber1)
}
```


## **Q4.3: Return method used for in a specific polymer synthesis experiment**
```sparql
SELECT DISTINCT ?experiment ?method ?method_type ?polymer
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:PolymerSynthesis.
  ?method rdf:type ?method_type .
  ?polymer_type rdfs:subClassOf* pmat:Polymer .
  ?polymer rdf:type ?polymer_type .
 
  ?experiment prov:generated ?polymer .
  ?experiment prov:used ?method .


  FILTER(?method=ex:synthesis32)
}
```

## **Q4.4: Return methods used for all membrane fabrication experiments**
```sparql
SELECT DISTINCT ?experiment ?method_type
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:MembraneFabrication.
  ?method rdf:type ?method_type .
  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?polymer_type .
 
  ?experiment prov:generated ?membrane .
  ?experiment prov:used ?method .
}
```

## **Q4.5: Return method used for a fabrication experiments of a specific membrane**
```sparql
SELECT DISTINCT ?experiment ?method_type ?membrane
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:MembraneFabrication.
  ?method rdf:type ?method_type .
  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?polymer_type .
 
  ?experiment prov:generated ?membrane .
  ?experiment prov:used ?method .
 
  FILTER(?membrane=ex:polytetrafluoroethyleneMembrane5)
}
```

## **Q4.6: Return methods used for a specific membrane fabrication experiment**
```sparql
SELECT DISTINCT ?experiment ?method ?method_type ?membrane
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:MembraneFabrication.
  ?method rdf:type ?method_type .
  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?polymer_type .
 
  ?experiment prov:generated ?membrane .
  ?experiment prov:used ?method .
 
  FILTER(?method=ex:fabrication56)
}
```

## **Q5.1: Return value of a specific parameter used in synthesis**
```sparql
SELECT DISTINCT ?quantity ?quantity_value ?quantity_unit ?method ?method_type
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:PolymerSynthesis.
  ?method rdf:type ?method_type .
  ?polymer_type rdfs:subClassOf* pmat:Polymer .
  ?polymer rdf:type ?polymer_type .
 
  ?experiment prov:generated ?polymer .
  ?experiment prov:used ?method .
 
  ?method bfo:RO_0000057 ?quantity .
  ?quantity om:hasValue ?quantity_measure .
  ?quantity_measure om:hasNumericalValue ?quantity_value .  
  ?quantity_measure om:hasUnit ?quantity_unit .
FILTER(?quantity=ex:massConcentrationOfIsobutyleneChloroform)
}
```

## **Q5.2: Return value of all parameters used in a specific synthesis**
```sparql
SELECT DISTINCT ?quantity ?quantity_value ?quantity_unit ?method ?method_type
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
  ?method_type rdfs:subClassOf* pmat:PolymerSynthesis.
  ?method rdf:type ?method_type .
  ?polymer_type rdfs:subClassOf* pmat:Polymer .
  ?polymer rdf:type ?polymer_type .
 
  ?experiment prov:generated ?polymer .
  ?experiment prov:used ?method .
 
  ?method bfo:RO_0000057 ?quantity .
  ?quantity om:hasValue ?quantity_measure .
  ?quantity_measure om:hasNumericalValue ?quantity_value .  
  ?quantity_measure om:hasUnit ?quantity_unit .
  FILTER(?method=ex:synthesis32)
}
```

## **Q6.1: Return value of a specific parameter used in preparation before analysis**
```sparql
SELECT DISTINCT ?quantity ?quantity_value ?quantity_unit ?preparation_method ?preparation_method_type
WHERE {

  ?experiment1_type rdfs:subClassOf* pmat:Experiment.
  ?experiment1 rdf:type ?experiment1_type .
  ?experiment2_type rdfs:subClassOf* pmat:Experiment.
  ?experiment2 rdf:type ?experiment2_type .
 
  ?preparation_method_type rdfs:subClassOf* pmat:SamplePreparation.
  ?preparation_method rdf:type ?preparation_method_type .
 
  ?analysis_type rdfs:subClassOf* pmat:Analysis.
  ?analysis rdf:type ?analysis_type .
 
  ?experiment1 prov:used ?preparation_method .
  ?experiment2 prov:used ?analysis .
  ?experiment1 bfo:RO_0004018 ?experiment2 .
 
  ?preparation_method bfo:RO_0000057 ?quantity .
  ?quantity om:hasValue ?quantity_measure .
  ?quantity_measure om:hasNumericalValue ?quantity_value .  
 ?quantity_measure om:hasUnit ?quantity_unit .
 FILTER(?quantity=ex:PtThickness26)
}
```

## **Q7.1: Return all membranes with their classes**
```sparql
SELECT DISTINCT ?membrane ?membrane_type
WHERE {

  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?membrane_type
}
```

## **Q7.2: Return membrane type of a specific membrane**
```sparql
SELECT DISTINCT ?membrane ?membrane_type
WHERE {

  ?membrane_type rdfs:subClassOf* pmat:Membrane .
  ?membrane rdf:type ?membrane_type

FILTER(?membrane=ex:polytetrafluoroethyleneMembrane5)
}
```

## **Q8.1: Return all modules with their classes**
```sparql
SELECT DISTINCT ?module ?module_type
WHERE {

  ?module_type rdfs:subClassOf* pmat:Module.
  ?module rdf:type ?module_type .
}
```

## **Q8.2: Return module type of a specific module**
```sparql
SELECT DISTINCT ?module ?module_type
WHERE {

  ?module_type rdfs:subClassOf* pmat:Module.
  ?module rdf:type ?module_type .

  FILTER(?module=ex:k100Module2)
}
```

## **Q9.1: Return persons who performed experiments**
```sparql
SELECT DISTINCT ?experiment ?person
WHERE
{
  ?experiment rdf:type pmat:Experiment .
   {
  ?person_type rdfs:subClassOf* prov:Person.
  ?person rdf:type ?person_type
  }
  UNION
  {
  ?person rdf:type prov:Person . }
  ?association rdf:type prov:Association .
  ?association prov:agent ?person .
  ?experiment prov:qualifiedAssociation ?association .
  ?experiment prov:wasAssociatedWith ?person .
  {
  ?role_type rdfs:subClassOf* pmat:User.
  ?role rdf:type ?role_type
  }
  UNION
  {
  ?role rdf:type pmat:User . }
  UNION
  {
  ?role_type rdfs:subClassOf* pmat:Maintainer.
  ?role rdf:type ?role_type
  }
  UNION
  {
  ?role rdf:type pmat:Maintainer . }
  UNION
  {
  ?role_type rdfs:subClassOf* pmat:Clerk.
  ?role rdf:type ?role_type
  }
  UNION
  {
  ?role rdf:type pmat:Clerk . }
  ?association prov:hadRole ?role .
}
```

## **Q10.1: Return equipment used in experiments**
```sparql
SELECT DISTINCT ?experiment ?equipment
WHERE
{
  ?experiment rdf:type pmat:Experiment
  {
  ?equipment_type rdfs:subClassOf* pmat:Device.
  ?equipment rdf:type ?equipment_type
  }
  UNION
  {
  ?equipment rdf:type pmat:Device . }
  ?experiment prov:used ?equipment .
}
```

## **Q11.1: Return recorded characteristics of membranes and polymers**
```sparql
SELECT DISTINCT ?object ?quantity ?object_type
WHERE
{
  {
  ?quantity_type rdfs:subClassOf* om:Quantity.
  ?quantity  rdf:type ?quantity_type .
  }
  UNION
  {
  ?quantity rdf:type om:Quantity . }
 
 
  {
  ?object_type rdfs:subClassOf* pmat:Polymer.
  ?object rdf:type ?object_type.
  }
  UNION
  { ?object rdf:type pmat:Polymer .}
  UNION
  {
  ?object_type rdfs:subClassOf* pmat:Membrane.
  ?object rdf:type ?object_type.
  }
  UNION
  { ?object rdf:type pmat:Membrane .}
  ?object bfo:RO_0000053 ?quantity .
  ?quantity om:hasValue ?value .
  ?value rdf:type pmat:Measure .
}
```

## **Q12.1: Return measured characteristics that calculated characteristics were derived from**
```sparql
SELECT DISTINCT ?measured_quantity ?calculated_quantity
WHERE
{
  {
  ?calculated_quantity_type rdfs:subClassOf* om:Quantity.
  ?calculated_quantity  rdf:type ?calculated_quantity_type .
  }
  UNION
  {
  ?measured_quantity rdf:type om:Quantity . }
  UNION
  {
  ?measured_quantity_type rdfs:subClassOf* om:Quantity.
  ?measured_quantity rdf:type ?measured_quantity_type . }
  UNION
  {
  ?measured_quantity rdf:type om:Quantity . }
  ?experiment rdf:type pmat:Experiment .
  ?experiment bfo:RO_0002234 ?measured_quantity .
  {
  ?calculation_type rdfs:subClassOf* pmat:Calculation.
  ?calculation rdf:type ?calculation_type.
  }
  UNION
  { ?calculation rdf:type pmat:Calculation .}
  ?calcuation bfo:RO_0002234 ?calculated_quantity .
  ?measured_quantity bfo:RO_0004018 ?calculated_quantity
}
```

## **Q13.1: Return characteristics of polymer or membranes measured in an experiment**
```sparql
SELECT DISTINCT ?object ?quantity ?method ?method_type
WHERE
{
  {
  ?quantity_type rdfs:subClassOf* om:Quantity.
  ?quantity rdf:type ?quantity_type. }
  UNION
  { ?quantity rdf:type om:Quantity .}
  OPTIONAL
  { ?quantity_type rdfs:label ?quantity_type_label .}
 OPTIONAL
 { ?quantity rdfs:label ?quantity_label . }
 ?experiment rdf:type pmat:Experiment .
 ?experiment bfo:RO_0002234 ?quantity .
 {?method_type rdfs:subClassOf* pmat:Method.
 ?method rdf:type ?method_type. }
 UNION
 { ?method rdf:type pmat:Method .}
 ?experiment  prov:used ?method .
 {
 ?object_type rdfs:subClassOf* pmat:Polymer.
 ?object rdf:type ?object_type.
 }
 UNION
 { ?object rdf:type pmat:Polymer .}
  UNION
 {
 ?object_type rdfs:subClassOf* pmat:Membrane.
 ?object rdf:type ?object_type. }
 UNION
 { ?object rdf:type pmat:Membrane .}
  OPTIONAL
 { ?object rdfs:label ?object_type .}
 ?object bfo:RO_0000053 ?quantity .
}
```

## **Q14.1: Return all characteristics that were calculated with names of subclasses**
```sparql
SELECT DISTINCT ?quantity ?calculation_type
WHERE
{
  {
  ?quantity_type rdfs:subClassOf* om:Quantity.
  ?quantity rdf:type ?quantity_type.
  }
  UNION
  { ?quantity rdf:type om:Quantity .}
  ?quantity bfo:RO_0002353 ?calculation  .
  {
  ?calculation_type rdfs:subClassOf* pmat:Calculation.
  ?calculation rdf:type ?calculation_type.
  }
  UNION
  { ?calculation rdf:type pmat:Calculation .}
  OPTIONAL
  {?calculation_type rdfs:label ?method_label}
}
```

## **Q14.2: Return calculated value a specific quantity**
```sparql
SELECT DISTINCT ?quantity ?calculation_type
WHERE
{
  {
  ?quantity_type rdfs:subClassOf* om:Quantity.
  ?quantity rdf:type ?quantity_type.
  }
  UNION
  { ?quantity rdf:type om:Quantity .}
  ?quantity bfo:RO_0002353 ?calculation  .
  {
  ?calculation_type rdfs:subClassOf* pmat:Calculation.
  ?calculation rdf:type ?calculation_type.
  }
  UNION
  { ?calculation rdf:type pmat:Calculation .}
  OPTIONAL
  {?calculation_type rdfs:label ?method_label}

  FILTER(?quantity=ex:modulusOfElasticity41)
}
```

## **Q15.1: Return storage location of all results of a calculation**
```sparql
SELECT DISTINCT ?calculation ?dataset ?result ?location ?path
WHERE
{
  {
  ?calculation_type rdfs:subClassOf* pmat:Calculation.
  ?calculation rdf:type ?calculation_type. 
  }
  UNION
  { ?calculation rdf:type pmat:Calculation .}
  ?calculation bfo:RO_0002234 ?dataset .
  ?dataset bfo:RO_0000053 ?outputdata .
  ?outputdata rdf:type pmat:OutputData .
  ?dataset bfo:RO_0000053 ?result .
  ?result rdf:type pmat:Result .
  ?dataset bfo:RO_0001025 ?location .
  OPTIONAL{?dataset pmat:hasPath ?path .}
}
```

## **Q16.1: Return software used in a given experiment**
```sparql
SELECT DISTINCT ?experiment ?software
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
 
 
  ?software_type rdfs:subClassOf* pmat:Software.
  ?software rdf:type ?software_type .
 
  ?experiment prov:used ?software .
 
 FILTER(?experiment=ex:experiment40)
}
```

## **Q16.2:  Return software used in a given calculation**
```sparql
SELECT DISTINCT ?calculation ?software
WHERE {

  ?calculation_type rdfs:subClassOf* pmat:Calculation.
  ?calculation rdf:type ?calculation_type .
 
 
  ?software_type rdfs:subClassOf* pmat:Software.
  ?software rdf:type ?software_type .
 
  ?calculation prov:used ?software .
 
 FILTER(?calculation=ex:arithmeticCalculation88)
}
```

## **Q17.1 Return models associated with a given experiment that were used in the experiment**
```sparql
SELECT DISTINCT ?experiment ?model
WHERE {

  ?experiment_type rdfs:subClassOf* pmat:Experiment.
  ?experiment rdf:type ?experiment_type .
 
 
  ?model_type rdfs:subClassOf* pmat:ComputationalModel.
  ?model rdf:type ?model_type .
 
  ?model bfo:RO_0003301 ?experiment .
  ?experiment bfo:RO_0002615 ?model .
  ?experiment prov:used ?model .
 
 FILTER(?experiment=ex:experiment40)
}
```

## **Q18.1: Return all requests with their submission date and time**
```sparql
SELECT DISTINCT ?request ?request_number ?submission_date_time
WHERE
{
?request pmat:hasOrderNumber ?request_number .
?request pmat:submissionStartedAt ?submission_date_time .
}
```

## **Q18.2: Return submission date and time of the request with a specific request number**
```sparql
SELECT DISTINCT ?request ?request_number ?submission_date_time
WHERE
{
?request pmat:hasOrderNumber ?request_number .
?request pmat:submissionStartedAt ?submission_date_time .
 
  FILTER(?request_number="2023000264")
}
```

## **Q19.1: Return submission date and time of a specific fabrication**
```sparql
SELECT DISTINCT ?request ?request_number ?request_type ?date
WHERE
{
?request pmat:hasOrderNumber ?request_number .
?request pmat:hasRequestSubmissionType ?request_type .
?request pmat:submissionStartedAt ?date .

FILTER(?request_number="2023000251" && ?request_type="fabrication")
}
```

## **Q19.2: Return submission date and time of the synthesis with a specific request number**
```sparql
SELECT DISTINCT ?request ?request_number ?request_type ?date
WHERE
{
?request pmat:hasOrderNumber ?request_number .
?request pmat:hasRequestSubmissionType ?request_type .
?request pmat:submissionStartedAt ?date .

FILTER(?request_number="2023000240" && ?request_type="synthesis")
}
```

## **Q19.3: Return all requests of type synthesis or fabrication with their submission date and time**
```sparql
SELECT DISTINCT ?request ?request_number ?request_type ?date
WHERE
{
?request pmat:hasOrderNumber ?request_number .
?request pmat:hasRequestSubmissionType ?request_type .
?request pmat:submissionStartedAt ?date .

FILTER(?request_type="synthesis" || ?request_type="fabrication")
}
```


